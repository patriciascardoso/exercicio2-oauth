package br.com.mastertech.contatos.controllers;


import br.com.mastertech.contatos.models.Contato;
import br.com.mastertech.contatos.security.Usuario;
import br.com.mastertech.contatos.services.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping ("/contato")
public class ContatoController {

    @Autowired
    private ContatoService contatoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Contato criarContato(@RequestBody @Valid Contato contato, @AuthenticationPrincipal Usuario usuario){
        return contatoService.criarContato(contato, usuario);
    }

    @GetMapping
    public List<Contato> consultarContatos(@AuthenticationPrincipal Usuario usuario){
      return contatoService.consultarContatos(usuario);
    }
}
