package br.com.mastertech.contatos.services;

import br.com.mastertech.contatos.models.Contato;
import br.com.mastertech.contatos.repositories.ContatoRepository;
import br.com.mastertech.contatos.security.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContatoService {


    @Autowired
    private ContatoRepository contatoRepository;

    public Contato criarContato(Contato contato, Usuario usuario){
        Contato contato1 = new Contato();
        contato1.setNome(contato.getNome());
        contato1.setNumero(contato.getNumero());
        contato1.setIdUsuario(usuario.getId());
        return contatoRepository.save(contato1);
    }

    public List<Contato> consultarContatos (Usuario usuario){
        return (List<Contato>) contatoRepository.findAllByIdUsuario(usuario.getId());
    }

}
